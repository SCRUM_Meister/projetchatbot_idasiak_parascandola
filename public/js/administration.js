const adminbotURL="http://localhost:8081/managebot";
const surveillanceURL="http://localhost:8081/surveillance";
const ecouteURL="http://localhost:8081/ecoute";
const invocation = new XMLHttpRequest();

document.getElementById('adminbot').addEventListener('click',adminbot);
document.getElementById('surveillance').addEventListener('click',surveillance);
document.getElementById('ecoute').addEventListener('click',ecoute);

// Aller vers l'interface de gestion des bots 
function adminbot(){
  console.log("test1");
  document.location=adminbotURL;
}

// Non implémentée
function surveillance(){
  console.log("test2");
  document.location=surveillanceURL;
}

// Non implémentée
function ecoute(){
  console.log("test3");
  document.location=ecouteURL;
}
