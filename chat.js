var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');

var RiveScript = require('rivescript');
var bot = new RiveScript();
var app = express();

//https://expressjs.com/en/resources/middleware/cors.html
app.use(cors());
var corsOptions = {
  origin: 'http://localhost:3000',
  methods: 'GET,POST',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

bot.loadFile('brain/default.rive').then(loading_done).catch(loading_error);


function loading_done(batch){
	bot.sortReplies();
}

function loading_error(error){

	console.log('error : ' + error);
}

let username ='test';

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());


// Gestion de l'envoie des messages
app.post('/message', cors(corsOptions), function(req, res) {


    if(req.is('json')) //on devrait toujours tester le type et aussi la taille!
    {
		  console.log(req.body);
		   let message = req.body.content;
        res.setHeader('Content-Type', 'application/json');
      	 bot.reply(username,message).then(
        (reply)=>{
        console.log(reply);
        	res.json(JSON.stringify(reply));
        	
        });
      
	  }else{
      res.send(400, 'Bad Request !');
    }

});

app.listen(8082, function(){
  console.log('listening on *:8082');
});


