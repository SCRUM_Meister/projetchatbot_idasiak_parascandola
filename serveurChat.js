 /**
 * Ceci est notre serveur.
 */

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
//var dao = require('dummyChatBotsDAO'); // in the node_modules directory

var http = require('http').Server(app);
const ChatBot = require('./public/js/ChatBot.js');
const ChatBots = require('./public/js/ChatBots.js');
const chatbots = new ChatBots();

var RiveScript = require('rivescript');
var bot = new RiveScript();
var listeApp = [];
var app = express();

//https://expressjs.com/en/resources/middleware/cors.html
app.use(cors());
var corsOptions = {
  origin: 'http://localhost:3000',
  methods: 'GET,POST,PUT,DELETE',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());



// Méthodes REST de création, modification et suppression des bots
//_______________________________________________________________\\


/** Récupère tout les chatbots**/
app.get('/manageBot', cors(corsOptions),function(req, res) {
  console.log("Affichage des chatbots");
  res.setHeader('Content-Type', 'application/json');
  res.json(chatbots.getChatBots());
});


/** Créer un chatbot**/
app.post('/manageBot', cors(corsOptions), function(req, res) {

    if(req.is('json')) //on devrait toujours tester le type et aussi la taille!
    {
		  var chatbot = chatbots.addChatBot(req.body);
        res.setHeader('Content-Type', 'application/json');
        res.json(chatbot);
        console.log("Done adding "+JSON.stringify(chatbot) );
	  }else{
      res.send(400, 'Bad Request !');
    }

});


/** Modifier un chatbot**/
app.put('/manageBot', cors(corsOptions), function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    if(req.is('json')) //on devrait toujours tester le type et aussi la taille!
    {
		    var chatbot = chatbots.updateChatBot(req.body);
        if(undefined==chatbot){
          res.send(404, 'Page introuvable !');
        }else{
        	res.json(chatbot);
        	console.log("Done updating "+JSON.stringify(chatbot) );
	}
    }else{
      res.send(400, 'Bad Request !');
    }

});


/** Supprimer un chatbot**/
app.delete('/manageBot/:id', cors(corsOptions),function(req, res) {
    let bot = chatbots.getChatBot(parseInt(req.params.id));
    let id = chatbots.deleteChatBot(parseInt(req.params.id));
	console.log("trouvé "+bot+" "+req.params.id+" hop");
	console.log("delete "+id+" "+req.params.id+" hop");
    if(undefined!=id){
        res.setHeader('Content-Type', 'text/plain');
	res.send(200,'OK');
    }else{
	res.send(404, 'Page introuvable !');
	}
});


//_______________________________________________________________\\

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.send(404, 'Page introuvable !');
});



app.listen(8080, function(err,data){
	if(err==undefined){
		console.log("app listening on *:8080");
	}else{
		console.log(`ERROR : ${err}`);
	}
});

