
 /**
 * Ceci est notre serveur client.
 */
var express = require('express');
var app = express();
var cors = require('cors');
var path = require('path');
app.set('view engine','ejs');
app.use(express.static(path.join(__dirname,'public')));
app.set('views',path.join(__dirname+'/public','views'));

//https://expressjs.com/en/resources/middleware/cors.html
app.use(cors());
var corsOptions = {
  origin: 'http://localhost:3000',
  methods: 'GET',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

/**
 * Chargement de la page principale d'administration.
 * @param {function} Redirige vers la page administration.ejs.
 */

app.get('/', function(req, res){
  res.render('administration');
  console.log('Connection à administration');
});

/**
 * Chargement de la page de gestion des chatBots.
 * @param {function} Redirige vers la page managebot.ejs.
 */
app.get('/managebot', function(req, res){
  res.render('managebot');
  console.log('Connection à managebot');
});

/**
 * Chargement de la page de chat.
 * @param {function} Redirige vers la page chat.ejs.
 */
 
app.get('/proxyChat', function(req, res){
 		res.render('proxyChat');
});

app.listen(8081, function(){
  console.log('listening on *:8081');
});
