class ChatBot{
  constructor(data){   //id,name,brain,interfaceCom,port
    if(undefined != data.id) {
      this.id = data.id;
    } else {
      this.id = parseInt(    Math.floor(Math.random() * Math.floor(100000))     );
    }
    if(undefined != data.name) {
      this.name = data.name;
    } else {
      this.name = "";
    }
    if(undefined != data.brain) {
      this.brain = data.brain;
    } else {
      this.brain = "default";
    }
    if(undefined != data.interfaceCom) {
      this.interfaceCom = data.interfaceCom;
    } else {
      this.interfaceCom = "local";
    }
    if(undefined != data.port) {
      this.port = data.port;
    } else {
      this.port = "";
    }
  }
}

module.exports = ChatBot;
