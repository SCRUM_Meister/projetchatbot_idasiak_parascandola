const managebotURL="http://localhost:8080/managebot";

document.getElementById('newbot').addEventListener('click',postBot);
document.getElementById('modifybot').addEventListener('click',putBot);
document.getElementById('delbot').addEventListener('click',delBot);
document.getElementById('showbot').addEventListener('click',getBot);

const invocation = new XMLHttpRequest();

// On initialise un bot
let newbot = {
      id: document.getElementById('idbot').value,
		name: document.getElementById('namebot').value,
		brain: document.getElementById('brain').value, 
		interfaceCom: document.getElementById('nameInterface').value,  
		port: document.getElementById('portbot').value
};

// On met à jour le bot avec les données de la page (dynamique)
function majBot(){
		console.log("Maj datas");
      newbot.id = parseInt(document.getElementById('idbot').value);
		newbot.name = document.getElementById('namebot').value;
		newbot.brain = document.getElementById('brain').value;
		newbot.interfaceCom = document.getElementById('nameInterface').value;
		newbot.port = document.getElementById('portbot').value;
}

// Gestion de la création d'un bot
function postBot(){
  console.log("newbot");
  majBot();
  if(invocation){
    invocation.open('POST', managebotURL, true);
    invocation.setRequestHeader('Content-Type', 'application/json');
    invocation.onreadystatechange = handler;
    invocation.send(JSON.stringify(newbot));
  }else{
    console.error("No Invocation TookPlace At All");
  }
}

// Gestion de l'affichage des bots
function getBot(){
  console.log("getbot");
  document.location=managebotURL;
}

// Gestion de la modification d'un bot
function putBot(){
  console.log("modifybot");
  majBot();
  if(invocation){
    invocation.open('PUT', managebotURL, true);
    invocation.setRequestHeader('Content-Type', 'application/json');
    invocation.onreadystatechange = handler;
    invocation.send(JSON.stringify(newbot));
  }else{
    console.error("No Invocation TookPlace At All");
  }
}

// Gestion de la supression d'un bot
function delBot(){
  console.log("delbot");
  majBot();
  if(invocation){
    let delBotURL = managebotURL + "/" + document.getElementById('idbot').value
    invocation.open('DELETE', delBotURL, true);
    invocation.onreadystatechange = handler;
    invocation.send(null);
  }else{
    console.error("No Invocation TookPlace At All");
  }
}

function handler(evtXHR){
  if (invocation.readyState == 4){
    if (invocation.status == 200){
	
	try{
      let response = JSON.parse(invocation.responseText);
		console.log(response);
	}catch(err){
		console.log("invocation.responseText "+invocation.responseText);	
	}
      
    }else{
      console.error("Invocation Errors Occured " + invocation.readyState + " and the status is " + invocation.status);
    }
  }else{
    console.log("currently the application is at" + invocation.readyState);
  }
}
