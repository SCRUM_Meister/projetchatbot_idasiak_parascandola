const messageURL="http://localhost:8082/message";

document.getElementById('send').addEventListener('click',send);

const invocation = new XMLHttpRequest();

function send() {
  console.log("envoi du message");
  let msg = {
  	"content": "hello",
  };
  console.log(msg);
  if(invocation){
    invocation.open('POST', messageURL, true);
    invocation.setRequestHeader('Content-Type', 'application/json');
    invocation.onreadystatechange = handler;
    invocation.send(JSON.stringify(msg));
  }else{
    console.error("No Invocation TookPlace At All");
  }
}



function handler(evtXHR){
  if (invocation.readyState == 4){
    if (invocation.status == 200){
	
	try{
	
      		let response = JSON.parse(invocation.responseText);
		console.log(response);
	}catch(err){
		console.log("invocation.responseText "+invocation.responseText);	
	}
      
    }else{
      console.error("Invocation Errors Occured " + invocation.readyState + " and the status is " + invocation.status);
    }
  }else{
    console.log("currently the application is at" + invocation.readyState);
  }
}
